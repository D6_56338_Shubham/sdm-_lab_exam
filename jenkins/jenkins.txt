Started by user Shubham Chandrashekhar Jadhav
Running as SYSTEM
Building in workspace C:\ProgramData\Jenkins\.jenkins\workspace\d6_Shubham_56338
The recommended git tool is: NONE
No credentials specified
Cloning the remote Git repository
Cloning repository https://gitlab.com/D6_56338_Shubham/sdm-_lab_exam.git
 > git.exe init C:\ProgramData\Jenkins\.jenkins\workspace\d6_Shubham_56338 # timeout=10
Fetching upstream changes from https://gitlab.com/D6_56338_Shubham/sdm-_lab_exam.git
 > git.exe --version # timeout=10
 > git --version # 'git version 2.34.1.windows.1'
 > git.exe fetch --tags --force --progress -- https://gitlab.com/D6_56338_Shubham/sdm-_lab_exam.git +refs/heads/*:refs/remotes/origin/* # timeout=10
 > git.exe config remote.origin.url https://gitlab.com/D6_56338_Shubham/sdm-_lab_exam.git # timeout=10
 > git.exe config --add remote.origin.fetch +refs/heads/*:refs/remotes/origin/* # timeout=10
Avoid second fetch
 > git.exe rev-parse "refs/remotes/origin/master^{commit}" # timeout=10
Checking out Revision 1698ccf49724e91b8723e585defb1fcb24779dd6 (refs/remotes/origin/master)
 > git.exe config core.sparsecheckout # timeout=10
 > git.exe checkout -f 1698ccf49724e91b8723e585defb1fcb24779dd6 # timeout=10
Commit message: "project for jenkins added"
First time build. Skipping changelog.
[d6_Shubham_56338] $ cmd /c call C:\Windows\TEMP\jenkins15539280951718198193.bat

C:\ProgramData\Jenkins\.jenkins\workspace\d6_Shubham_56338>docker-compose down 
Found orphan containers (d6_shubham_56338_server_1, d6_shubham_56338_bookdb_1) for this project. If you removed or renamed this service in your compose file, you can run this command with the --remove-orphans flag to clean it up.
Removing network d6_shubham_56338_default
error while removing network: network d6_shubham_56338_default id b2b41a8a705bbf87cd04889d8b9c6128678c9ac0593e355948102d9ac5f2b82e has active endpoints

C:\ProgramData\Jenkins\.jenkins\workspace\d6_Shubham_56338>docker-compose build 
Building demodb
#1 [internal] load build definition from Dockerfile
#1 sha256:3f79cd8ab255a73e0944f2a0cec9036ebca21e4b081857d332276765b0ede274
#1 transferring dockerfile: 173B 0.0s done
#1 DONE 0.0s

#2 [internal] load .dockerignore
#2 sha256:5bf0042a76f14d07c16df11384ebd1465a4e2abc53c1564661468b3722181b47
#2 transferring context: 2B done
#2 DONE 0.0s

#3 [internal] load metadata for docker.io/library/mysql:latest
#3 sha256:f4914bf881bba81f49673260093dead2b629e18e4d67c76939b1f813bb051069
#3 DONE 0.0s

#4 [1/2] FROM docker.io/library/mysql
#4 sha256:a5d39839c39cd4bd7fb960856a647e2bb12c7b06ec949f6bf97790e3436e389c
#4 DONE 0.0s

#5 [internal] load build context
#5 sha256:b6f057c588ccbcd69ea4a7a36e9961ced13827c4afdfb34964240472653b539b
#5 transferring context: 738B done
#5 DONE 0.0s

#6 [2/2] COPY db.sql /docker-entrypoint-initdb.d/
#6 sha256:31c5df665376f1caa35677d0d03da9bf9a62705bfc4b6c743b1798a16a819d36
#6 CACHED

#7 exporting to image
#7 sha256:e8c613e07b0b7ff33893b694f7759a10d42e180f2b4dc349fb57dc6b71dcab00
#7 exporting layers done
#7 writing image sha256:812944cdcc8234d2afba9342f032b7b3c5a0b366d9c84b4f917e55b921386cfe done
#7 naming to docker.io/library/d6_shubham_56338_demodb done
#7 DONE 0.0s

Use 'docker scan' to run Snyk tests against images to find vulnerabilities and learn how to fix them
Building product-service
#1 [internal] load build definition from Dockerfile
#1 sha256:95dadf6bdf3579cba2b9f401a9db947ddc5cea78d98d6eda8bc7acd0df9c617e
#1 transferring dockerfile: 229B 0.0s done
#1 DONE 0.0s

#2 [internal] load .dockerignore
#2 sha256:124fc84aad8adad0cf995d1852c0e9b8176110bbdf01911010290f1232fc837f
#2 transferring context: 2B done
#2 DONE 0.0s

#3 [internal] load metadata for docker.io/library/node:latest
#3 sha256:1c0b05b884068c98f7acad32e4f7fd374eba1122b4adcbb1de68aa72d5a6046f
#3 DONE 0.0s

#4 [1/4] FROM docker.io/library/node
#4 sha256:5045d46e15358f34ea7fff145af304a1fa3a317561e9c609f4ae17c0bd3359df
#4 DONE 0.0s

#6 [internal] load build context
#6 sha256:5b64f71dc115f20caafb82c27420f38863e151a1b7139735f32097ba478a7a57
#6 transferring context: 28.77kB done
#6 DONE 0.0s

#5 [2/4] WORKDIR /app
#5 sha256:9c069f6fc249db0a1d261845aed20bc20c1a5fdae3d2cbc7f8f5ef59eb080214
#5 CACHED

#7 [3/4] COPY . .
#7 sha256:c2f61f56543204605e153514b317155a1b055a13d5139046a81ab8bc5495762c
#7 CACHED

#8 [4/4] RUN npm install
#8 sha256:4e6e9b6849073e8a84cfb40800596972af5e9dc651b7e7d6aa053a76ce9ae901
#8 CACHED

#9 exporting to image
#9 sha256:e8c613e07b0b7ff33893b694f7759a10d42e180f2b4dc349fb57dc6b71dcab00
#9 exporting layers done
#9 writing image sha256:a524637a7945b44b35f01d8d1712f8510877443a482297315ff6329c9f088509 done
#9 naming to docker.io/library/d6_shubham_56338_product-service done
#9 DONE 0.0s

Use 'docker scan' to run Snyk tests against images to find vulnerabilities and learn how to fix them
Building category-service
#1 [internal] load build definition from Dockerfile
#1 sha256:67001bc24b0e46c2d4e101f68b56f89b215d9a715f90cb7d1b8864671bf5f2ee
#1 transferring dockerfile: 229B 0.0s done
#1 DONE 0.0s

#2 [internal] load .dockerignore
#2 sha256:da0feb7ee3332e5cabc06c446fbd4c99c650c7ffd9f8c3b2867ddbe7aae83585
#2 transferring context: 2B done
#2 DONE 0.0s

#3 [internal] load metadata for docker.io/library/node:latest
#3 sha256:1c0b05b884068c98f7acad32e4f7fd374eba1122b4adcbb1de68aa72d5a6046f
#3 DONE 0.0s

#4 [1/4] FROM docker.io/library/node
#4 sha256:5045d46e15358f34ea7fff145af304a1fa3a317561e9c609f4ae17c0bd3359df
#4 DONE 0.0s

#6 [internal] load build context
#6 sha256:b8a29c0cf549b43b445d091af29a41eb77e3bdb3f836034fe3b38eb87ad823a6
#6 transferring context: 28.61kB 0.0s done
#6 DONE 0.0s

#5 [2/4] WORKDIR /app
#5 sha256:9c069f6fc249db0a1d261845aed20bc20c1a5fdae3d2cbc7f8f5ef59eb080214
#5 CACHED

#7 [3/4] COPY . .
#7 sha256:3fdc1918c88c4b62b28ce4bf3e23a48a3c58c67b7073b22841f9b71858435c64
#7 CACHED

#8 [4/4] RUN npm install
#8 sha256:c7d18b303d67b2f9f93728ecde9d64ea498a7c626ac51ad1cbbcb8dda34da477
#8 CACHED

#9 exporting to image
#9 sha256:e8c613e07b0b7ff33893b694f7759a10d42e180f2b4dc349fb57dc6b71dcab00
#9 exporting layers done
#9 writing image sha256:0d61e0eab5802d6fdd2dad38e2420ce844673057082544dca306603449c8f0ac done
#9 naming to docker.io/library/d6_shubham_56338_category-service done
#9 DONE 0.0s

Use 'docker scan' to run Snyk tests against images to find vulnerabilities and learn how to fix them

C:\ProgramData\Jenkins\.jenkins\workspace\d6_Shubham_56338>docker-compose up -d 
Found orphan containers (d6_shubham_56338_bookdb_1, d6_shubham_56338_server_1) for this project. If you removed or renamed this service in your compose file, you can run this command with the --remove-orphans flag to clean it up.
Creating d6_shubham_56338_category-service_1 ... 

Creating d6_shubham_56338_product-service_1  ... 

Creating d6_shubham_56338_demodb_1           ... 

Creating d6_shubham_56338_product-service_1  ... done

Creating d6_shubham_56338_demodb_1           ... done

Creating d6_shubham_56338_category-service_1 ... done


C:\ProgramData\Jenkins\.jenkins\workspace\d6_Shubham_56338>exit 0 
Finished: SUCCESS